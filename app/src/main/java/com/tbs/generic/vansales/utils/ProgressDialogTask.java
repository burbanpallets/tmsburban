package com.tbs.generic.vansales.utils;

import android.content.Context;

import com.alert.rajdialogs.ProgressDialog;

public class ProgressDialogTask {
    private ProgressDialog progressDialog;
    private static ProgressDialogTask instance;

    private ProgressDialogTask() {
    }

    public static ProgressDialogTask getInstance() {
        if (instance == null) {
            instance = new ProgressDialogTask();
        }
        return instance;
    }

    public void showProgress(Context mContext, boolean isCancalable, String mMessage) {
        progressDialog = Util.showProgressDialog(mContext);
        progressDialog.show();
    }

    public void closeProgress() {
        if (progressDialog != null)
            progressDialog.hide();
    }
}
