package com.tbs.generic.vansales.Activitys

import android.app.Dialog
import android.content.Intent
import android.os.Environment
import androidx.appcompat.app.AppCompatDialog
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.io.File

//
class SalesPODActivity : BaseActivity() {
    lateinit var alertbox2: Dialog
    lateinit var messageTextView2: TextView
    lateinit var confirmBtn2: Button
    lateinit var cancelBtn2: Button
    lateinit var customerId: String
    lateinit var stringBuilder: StringBuilder
    override fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.sales_pod, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()

        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras?.getString("CustomerId")!!
        }
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }


    }

    override fun initializeControls() {
        tvScreenTitleTop.visibility = View.VISIBLE
        tvScreenTitleBottom.visibility = View.VISIBLE

        var a = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        var b = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "")


        val dateTimeA = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT_AT, "")
        val dateTimeD = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT_DT, "")

        tvScreenTitleTop.text = "" + a
        tvScreenTitleBottom.text = "" + b

        val btnConfirm = findViewById<Button>(R.id.btnConfirmDeparture)
        val tvATime = findViewById<TextView>(R.id.tvATime)
        val tvATimeD = findViewById<TextView>(R.id.tvATimeD)
        val tvETimeD = findViewById<TextView>(R.id.tvETimeD)
        val tvETime = findViewById<TextView>(R.id.tvETime)

        tvATime.text = ""
        tvATimeD.text = ""

        tvETimeD.text = "" + dateTimeD
        tvETime.text = "" + dateTimeA

        btnConfirm.setOnClickListener {
            Util.preventTwoClick(it)
            // this.actualDepartureTime = this.df.format(Date())
            alertbox2 = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            alertbox2.requestWindowFeature(1)
            alertbox2.setContentView(R.layout.dialog_simple_alert)
            messageTextView2 = alertbox2.findViewById(R.id.tvMessage) as TextView
            confirmBtn2 = alertbox2.findViewById(R.id.btnOk) as Button
            cancelBtn2 = alertbox2.findViewById(R.id.btnNo) as Button
            (alertbox2.findViewById(R.id.tvTitle) as TextView).text = getString(R.string.confirm_depature_from_location)
            stringBuilder = StringBuilder()
            stringBuilder.append("Have you reached " + b + "\n on ")
            stringBuilder.append(CalendarUtils.getCurrentDate(this))
            stringBuilder.append(" ?")
            messageTextView2.text = stringBuilder.toString()
            cancelBtn2.setOnClickListener {
                Util.preventTwoClick(it)
                alertbox2.dismiss() }
            confirmBtn2.setOnClickListener {
                Util.preventTwoClick(it)
                //  this@PODActivity.actualDepatureDate.setText(this@PODActivity.actualDepartureTime)
                alertbox2.dismiss()
                tvATimeD.text = "" + CalendarUtils.getCurrentDate(this)

                //Deleting signature after submit
                var files = File(Environment.getExternalStorageDirectory().path + "/UserSignature/signature.png")
                if (files.exists()) {
                    files.delete()
                }

            }
            alertbox2.show()
        }


        val btnConfirmArrival = findViewById<Button>(R.id.btnConfirmArrival)

        btnConfirmArrival.setOnClickListener {

            // this.actualArrivalTime = this.df.format(Date())
            alertbox2 = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            alertbox2.requestWindowFeature(1)
            alertbox2.setContentView(R.layout.dialog_simple_alert)
            messageTextView2 = alertbox2.findViewById(R.id.tvMessage) as TextView
            confirmBtn2 = alertbox2.findViewById(R.id.btnOk) as Button
            cancelBtn2 = alertbox2.findViewById(R.id.btnNo) as Button
            (alertbox2.findViewById(R.id.tvTitle) as TextView).text =getString(R.string.confirm_arrval_at_location)
            stringBuilder = StringBuilder()
            stringBuilder.append("Have you reached " + b + "\n on ")
            stringBuilder.append(dateTimeA)
            stringBuilder.append(" ?")
            messageTextView2.text = stringBuilder.toString()
            cancelBtn2.setOnClickListener(View.OnClickListener {
                Util.preventTwoClick(it)
                alertbox2.dismiss() })
            confirmBtn2.setOnClickListener(View.OnClickListener {
                Util.preventTwoClick(it)
                // this@PODActivity.actualArrivalDate.setText(this@PODActivity.actualArrivalTime)
                alertbox2.dismiss()
            })
            alertbox2.show()
        }
        val btnDeliveryDetails = findViewById<Button>(R.id.btnDeliveryDetails)

        btnDeliveryDetails.setOnClickListener {
            Util.preventTwoClick(it)
            //            val intent = Intent(this@SalesPODActivity, CaptureDeliveryActivity::class.java);
//            startActivity(intent);
        }
        val btnEndUnloading = findViewById<Button>(R.id.btnEndUnloading)

        btnEndUnloading.setOnClickListener {
            Util.preventTwoClick(it)
            //            val intent = Intent(this@PODActivity, DashBoardActivity::class.java);
//            startActivity(intent);
        }

        val btnUnloading = findViewById<Button>(R.id.btnUnloading)

        btnUnloading.setOnClickListener{
            Util.preventTwoClick(it)
            //            val intent = Intent(this@PODActivity, DashBoardActivity::class.java);
//            startActivity(intent);
            tvATime.text = "" + CalendarUtils.getCurrentDate(this)

        }
        val btnPayments = findViewById<Button>(R.id.btnGenerate)

        btnPayments.setOnClickListener{
            Util.preventTwoClick(it)
            val intent = Intent(this@SalesPODActivity, PaymentsActivity::class.java)
            startActivity(intent)
        }
        val btnPrintDocs = findViewById<Button>(R.id.btnPrintDocuments)

        btnPrintDocs.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this@SalesPODActivity, PrintDocumentsActivity::class.java)
            startActivity(intent)
        }
    }

}