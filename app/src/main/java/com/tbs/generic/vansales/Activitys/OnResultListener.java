package com.tbs.generic.vansales.Activitys;

import com.tbs.generic.vansales.Model.CylinderIssueMainDO;

public interface OnResultListener {
        void onCompleted(boolean isError, CylinderIssueMainDO cylinderIssueMainDO);

    }