package com.tbs.generic.vansales.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.MenuModel
import java.util.*

class ExpandableListAdapter(private val context: Context, private val listDataHeader: List<MenuModel>, private val listDataChild: HashMap<MenuModel, List<MenuModel>>) : BaseExpandableListAdapter() {
    lateinit var mcontext :Context

    override fun getChild(groupPosition: Int, childPosititon: Int): MenuModel {
        return (this.listDataChild[this.listDataHeader[groupPosition]] as List<*>)[childPosititon] as MenuModel
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    @SuppressLint("WrongConstant")
    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        var childText = getChild(groupPosition, childPosition).menuName
        if (convertView == null) {
            convertView = (this.context.getSystemService("layout_inflater") as LayoutInflater).inflate(R.layout.list_group_child, null)
        }
        (convertView!!.findViewById<View>(R.id.tvItem) as TextView).text = childText
        var ivDoneTick :ImageView = (convertView.findViewById<View>(R.id.ivDoneTick) as ImageView)
        var vehicleCheckinDO=StorageManager.getInstance(context).getVehicleCheckInData(context)

//        var baseActivity : BaseActivity? = this.context as BaseActivity?
//        var status = baseActivity!!.preferenceUtils.getStringFromPreference(PreferenceUtils.CHECK_IN_STATUS, "");
        var status = StorageManager.getInstance(context).getVehicleCheckInData(context).checkInStatus
        if(childText.equals("Vehicle Check in") && status.equals("Vehicle Checked-In")) {
            ivDoneTick.visibility = View.VISIBLE
        }
        else  if(childText.equals("Load Van Sales Stock") && vehicleCheckinDO.loadStock.equals(context.resources.getString(R.string.loaded_stock))) {
            ivDoneTick.visibility = View.VISIBLE


        }
        else{
            ivDoneTick.visibility = View.GONE
        }
        return convertView
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return if (this.listDataChild[this.listDataHeader[groupPosition]] == null) {
            0
        } else (this.listDataChild[this.listDataHeader[groupPosition]] as List<*>).size
    }

    override fun getGroup(groupPosition: Int): MenuModel {
        return this.listDataHeader[groupPosition]
    }

    override fun getGroupCount(): Int {
        return this.listDataHeader.size
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    @SuppressLint("WrongConstant")
    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val headerTitle = getGroup(groupPosition).menuName
        if (convertView == null) {
            convertView = (this.context.getSystemService("layout_inflater") as LayoutInflater).inflate(R.layout.list_group_header, null)
        }
        val lblListHeader = convertView!!.findViewById<View>(R.id.tvHeader) as TextView
        lblListHeader.setTypeface(null, 1)
        lblListHeader.text = headerTitle
        val ivArrow = convertView.findViewById<View>(R.id.ivArrow)
        ivArrow.rotation = 90.0f
        if (getChildrenCount(groupPosition) == 0) {
            ivArrow.visibility = View.INVISIBLE
        }
        else {
            ivArrow.visibility = View.VISIBLE
        }

        return convertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    companion object {
        private val EMPTY_STATE_SET = IntArray(0)
        private val GROUP_EXPANDED_STATE_SET = intArrayOf(16842920)
        private val GROUP_STATE_SETS = arrayOf(EMPTY_STATE_SET, GROUP_EXPANDED_STATE_SET)
    }
}
