package com.tbs.generic.vansales.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Adapters.CustomerAdapter;
import com.tbs.generic.vansales.Adapters.RouteIdsEMAdapter;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.Model.DriverIdMainDO;
import com.tbs.generic.vansales.Model.RouteDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.Requests.DriverIdRequest;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.List;

/**
 * Created by sandy on 2/15/2018.
 */

public class NonAssignedFragmment extends Fragment  {
    private String userId,orderCode;
    private PreferenceUtils preferenceUtils;
    private RecyclerView recycleview;
    private CustomerAdapter orderHistoryAdapter;
    private RelativeLayout llOrderHistory;
    private TextView tvNoOrders;
    private List<RouteDO> customerDos;

DriverIdMainDO driverIdMainDo;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.non_assigned_route_list, container, false);

        // userId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
        recycleview = rootView. findViewById(R.id.recycleview);
        CustomerDo orderHistoryDo = new CustomerDo();
        preferenceUtils = new PreferenceUtils(getActivity());

        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
           if(id.length()>0){
               recycleview.setVisibility(View.VISIBLE);
               DriverIdRequest driverListRequest = new DriverIdRequest(id,getActivity());
               driverListRequest.setOnResultListener(new DriverIdRequest.OnResultListener() {
                   @Override
                   public void onCompleted(boolean isError, DriverIdMainDO driverIdMainDO) {
                       driverIdMainDo = driverIdMainDO;
                       ((BaseActivity)getActivity()).hideLoader();

                       if (isError) {
                           Toast.makeText(getActivity(), "Failed to get route list", Toast.LENGTH_SHORT).show();
                       } else {

                           RouteIdsEMAdapter driverAdapter = new RouteIdsEMAdapter(getActivity(), driverIdMainDO.driverEmptyDOS);
                           recycleview.setLayoutManager(new LinearLayoutManager(getActivity()));

                           recycleview.setAdapter(driverAdapter);

                       }


                   }
               });
               driverListRequest.execute();
           }else {
               recycleview.setVisibility(View.GONE);
           }


        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);


        return rootView;
    }
}