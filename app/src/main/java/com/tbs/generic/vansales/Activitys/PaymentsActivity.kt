package com.tbs.generic.vansales.Activitys

import android.content.Intent
import android.text.TextUtils
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO
import com.tbs.generic.vansales.Model.CustomerDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.AuthorizationListner
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import com.tbs.generic.vansales.listeners.ResultListner


//
class PaymentsActivity : BaseActivity() {

    lateinit var customerDo: CustomerDo
    lateinit var activeDeliverySavedDo: ActiveDeliveryMainDO
    lateinit var ShipmentType: String
    var customer = ""
    lateinit var btnInvoice: LinearLayout
    lateinit var btnPayments: LinearLayout
    lateinit var btnCompleted: Button
    lateinit var btnAccountPayments: LinearLayout
    var ticket = ""


    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.payments, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        tvScreenTitle.setText(R.string.payments)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        initializeControls()
        if (intent.hasExtra("PICK_TICKET")) {
            ticket = intent.extras?.getString("PICK_TICKET")!!
        }

        activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
        ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))

        if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
            customer = customerDo.customer
        } else {
            customer = activeDeliverySavedDo.customer
        }
        btnInvoice.setOnClickListener {
            Util.preventTwoClick(it)
            customerAuthorization(customer, 7, ResultListner { `object`, isSuccess ->
                if (isSuccess) {
                    var intent = Intent(this@PaymentsActivity, CreateInvoiceActivity::class.java)
                    intent.putExtra("PICK_TICKET", ticket)
                    startActivity(intent)
                } else {
                    showAlert(resources.getString(R.string.not_authorized))

                }
            })
        }
        btnPayments.setOnClickListener {

            customerAuthorization(customer, 22, ResultListner { `object`, isSuccess ->

                if (isSuccess) {
                    Util.preventTwoClick(it)
                    var intent = Intent(this@PaymentsActivity, InvoiceListActivity::class.java)
                    startActivity(intent)
                } else {
                    showAlert(resources.getString(R.string.not_authorized))

                }
            })

        }
        btnAccountPayments.setOnClickListener {
            customerAuthorization(customer, 22, ResultListner { `object`, isSuccess ->

                if (isSuccess) {
                    Util.preventTwoClick(it)
                    var intent = Intent(this@PaymentsActivity, OnAccountCreatePaymentActivity::class.java)
                    startActivity(intent)
                } else {
                    showAlert(resources.getString(R.string.not_authorized))

                }
            })


        }
        btnCompleted.setOnClickListener {
            Util.preventTwoClick(it)
            showAppCompatAlert("", getString(R.string.are_you_sure_you_want_to_exit), getString(R.string.ok), getString(R.string.cancel), getString(R.string.success), true)

        }
    }


    override fun initializeControls() {
        btnAccountPayments = findViewById<LinearLayout>(R.id.btnAccountPayment)
        btnInvoice = findViewById<LinearLayout>(R.id.btnInvoice)
        btnPayments = findViewById<LinearLayout>(R.id.btnPayments)
        btnCompleted = findViewById<Button>(R.id.btnCompleted)
    }

    override fun onButtonNoClick(from: String) {

    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {

            setResult(2, null)
            finish()
        } else
            if (getString(R.string.failure).equals(from, ignoreCase = true)) {


                //finish()
            }

    }

    override fun onResume() {
        super.onResume()
        var data = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "")
        if (!TextUtils.isEmpty(data)) {
            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnInvoice.isClickable = false
            btnInvoice.isEnabled = false

        }
    }
}