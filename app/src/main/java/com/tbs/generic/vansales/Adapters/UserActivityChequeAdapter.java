package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.ChequeDO;
import com.tbs.generic.vansales.R;

import java.util.ArrayList;

public class UserActivityChequeAdapter extends RecyclerView.Adapter<UserActivityChequeAdapter.MyViewHolder> {

    private ArrayList<ChequeDO> chequeDOS;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRecieptNumber, tvCustomerName;
        private TextView tvAmount;

        public MyViewHolder(View view) {
            super(view);
            tvAmount = view.findViewById(R.id.tvAmount);
            tvRecieptNumber = view.findViewById(R.id.tvRecieptNumber);
            tvCustomerName = view.findViewById(R.id.tvCustomerName);



        }
    }


    public UserActivityChequeAdapter(Context context, ArrayList<ChequeDO> siteDOS) {
        this.context = context;
        this.chequeDOS = siteDOS;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_data_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final ChequeDO chequeDO = chequeDOS.get(position);

        String amount = String.valueOf(chequeDO.amount);

        holder.tvRecieptNumber.setText(chequeDO.paymentId);
        holder.tvCustomerName.setText(chequeDO.customerName);
        holder.tvAmount.setText(""+amount);




        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });


    }

    @Override
    public int getItemCount() {
        return chequeDOS.size();
    }

}
