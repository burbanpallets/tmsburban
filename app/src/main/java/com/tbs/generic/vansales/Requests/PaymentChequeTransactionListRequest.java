package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.TransactionCashDO;
import com.tbs.generic.vansales.Model.TransactionChequeDO;
import com.tbs.generic.vansales.Model.TransactionChequeMainDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class PaymentChequeTransactionListRequest extends AsyncTask<String, Void, Boolean> {

    private TransactionChequeMainDo transactionMainDo;
    private TransactionCashDO transactionCashDO;
    private TransactionChequeDO transactionChequeDO;

    private Context mContext;
    PreferenceUtils preferenceUtils;
    String startDATE, endDATE;


    public PaymentChequeTransactionListRequest(String startdate, String endDate, Context mContext) {

        this.mContext = mContext;
        this.startDATE = startdate;
        this.endDATE = endDate;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, TransactionChequeMainDo unPaidInvoiceMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("I_YUSER", id);
            jsonObject.put("I_YSTARTDATE", startDATE);
            jsonObject.put("I_ENDDATE", endDATE);
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.PAYMENTS_CHEQUE, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            transactionMainDo = new TransactionChequeMainDo();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP5")) {

                    } else if (startTag.equalsIgnoreCase("GRP2")) {
                        transactionMainDo.transactionChequeDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("TAB")) {

                        if (startTag.equalsIgnoreCase("GRP6")) {

                        }
                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        transactionChequeDO = new TransactionChequeDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_CUST")) {
                            transactionChequeDO.chequeCustomer = text;


                        } else if (attribute.equalsIgnoreCase("O_AMT")) {
                            if (text.length() > 0) {

                                transactionChequeDO.chequeAmount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_PAYNUM")) {
                            if (text.length() > 0) {

                                transactionChequeDO.chequePayment = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_PAYTYP")) {
                            if (text.length() > 0) {

                                transactionChequeDO.payType2 = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YTOTCHQPAY")) {
                            if (text.length() > 0) {

                                transactionMainDo.chequeReciepts = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YTOTCHQAMT")) {
                            if (text.length() > 0) {

                                transactionMainDo.totalAmount = Double.valueOf(text);
                            }

                        }
                        text = "";

                    }

//                    if (endTag.equalsIgnoreCase("GRP5")) {
//                        if (endTag.equalsIgnoreCase("LIN")) {
//                            transactionMainDo.transactionCashDOS.add(transactionCashDO);
//
//                        }
//                    }


                    if (endTag.equalsIgnoreCase("LIN")) {

                        transactionMainDo.transactionChequeDOS.add(transactionChequeDO);

                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    if (xpp.getText().length() > 0) {

                        text = xpp.getText();
                    } else {
                        text = "";
                    }
                }


                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, transactionMainDo);
        }
    }
}