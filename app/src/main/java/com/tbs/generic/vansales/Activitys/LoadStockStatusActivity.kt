package com.tbs.generic.vansales.Activitys


import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Adapters.LoadStockAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.NewLoadVanSaleRequest
import com.tbs.generic.vansales.Requests.NonScheduledLoadVanSaleRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.fragments.OpenStockFragment
import com.tbs.generic.vansales.fragments.VehicleStockFragment
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.Constants
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.activity_load_stock_status.*
import kotlinx.android.synthetic.main.configuration_layout.*
import java.util.ArrayList

class LoadStockStatusActivity : BaseActivity(),
        OpenStockFragment.OnFragmentInteractionListener,
        VehicleStockFragment.OnFragmentInteractionListener {


    private var rlPartyActivity: RelativeLayout? = null

    private var nonScheduledRootId: String = ""
    private var availableStockDos: ArrayList<LoadStockDO> = ArrayList()
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView

    //    lateinit var loadStockMainDO : LoadStockMainDO
    var flag: Int = 0

    private var scheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()

    override fun initialize() {
        rlPartyActivity = layoutInflater.inflate(R.layout.activity_load_stock_status, null) as RelativeLayout
        llBody.addView(rlPartyActivity, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()
        /*toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }*/
        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        initializeControls()
        if (intent.hasExtra(Constants.SCREEN_TYPE)) {
            tv_title.text = intent.extras!!.getString(Constants.SCREEN_TYPE)

        } else {
            tv_title.text = getString(R.string.stock_)
        }


        /* toolbar.setNavigationIcon(R.drawable.back)
         toolbar.setNavigationOnClickListener {
             finish()
 //            overridePendingTransition(R.anim.exit, R.anim.enter)
         }*/


//        tabLayout.addTab(tabLayout.newTab().setText("Available Stock"))

        /* if (intent.hasExtra("FLAG")) {
             flag = intent.extras.getInt("FLAG")
         }
         if (flag == 1) {*/
        Util.startNewFragment(this, R.id.container, VehicleStockFragment.newInstance(), "VehicleStockFragment", false)
        //}
        /*else  if(flag == 2){
                btnLoadVehicle.setVisibility(View.VISIBLE)
                btnLoadVehicle.setText("UnLoad Vehicle")
                btnLoadReject.setVisibility(View.GONE)

            }
            else {
                btnLoadReject.setVisibility(View.GONE)
                btnLoadVehicle.setVisibility(View.VISIBLE)
            }
            val vehicleCheckInDo = StorageManager.getInstance(this@LoadStockStatusActivity).getVehicleCheckInData(this@LoadStockStatusActivity);
            scheduledRootId = vehicleCheckInDo.scheduledRootId!!
            nonScheduledRootId = vehicleCheckInDo.nonScheduledRootId!!

            if(!scheduledRootId.equals("", true)){
    //            scheduleDos = StorageManager.getInstance(this).getVanScheduleProducts(this);
            }
            if(!nonScheduledRootId.equals("", true)){
    //            nonScheduleDos = StorageManager.getInstance(this).getVanNonScheduleProducts(this);
            }

            if(scheduleDos == null || scheduleDos!!.size<1){
                loadVehicleStockData()
            }
            else if(nonScheduleDos == null || nonScheduleDos!!.size<1){
                loadNonScheduledVehicleStockData()
            }
            else{
                var isProductExisted = false;

                availableStockDos = nonScheduleDos
                for (i in scheduleDos.indices) {
                    for (k in availableStockDos.indices) {
                        if (scheduleDos.get(i).product.equals(availableStockDos!!.get(k).product, true)) {
                            availableStockDos!!.get(k).quantity = availableStockDos!!.get(k).quantity + scheduleDos.get(i).quantity
                            isProductExisted = true
                            break
                        }
                    }
                    if (isProductExisted) {
                        isProductExisted = false
                        continue
                    } else {
                        availableStockDos!!.add(scheduleDos.get(i))
                    }
                }
                if (availableStockDos != null && availableStockDos.size > 0) {
                    var loadStockAdapter = LoadStockAdapter(this@LoadStockStatusActivity, availableStockDos, "Shipments")
                    recycleview.setAdapter(loadStockAdapter)
                }
            }
            btnLoadVehicle.setOnClickListener {
                var id = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
                var nID = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

                if(id.isEmpty()&&nID.isEmpty()){
                  showAlert("Stock Not Assigned to Driver")
                  }else{

                      showAppCompatAlert("", "Are You Sure You Want to \n confirm load?", "OK", "Cancel", "SUCCESS", true)
                  }


            }*/
    }

    override fun initializeControls() {
        /*   btnLoadVehicle          = findViewById<Button>(R.id.btnLoadVehicle) as Button

           btnLoadReject          = findViewById<Button>(R.id.btnLoadReject) as Button
           recycleview = findViewById<View>(R.id.recycleview) as RecyclerView
           recycleview.setLayoutManager(LinearLayoutManager(this))

           btnLoadReject.setOnClickListener {
                  setResult(10, null)
                  finish()
              }*/
    }

//    private fun loadVehicleStockData() {
//        if (Util.isNetworkAvailable(this)) {
//            var scheduledRootId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
//
//            if (!scheduledRootId.equals("", true)) {
//                val loadVanSaleRequest = NewLoadVanSaleRequest(scheduledRootId, this@LoadStockStatusActivity)
//                showLoader()
//                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
//                    hideLoader()
//                    if (isError) {
//                        showToast(getString(R.string.no_vehicle_data_found))
//                    } else {
//                        scheduleDos = loadStockMainDo.loadStockDOS
//
//                    }
//                    loadNonScheduledVehicleStockData()
//                }
//                loadVanSaleRequest.execute()
//            } else {
//                loadNonScheduledVehicleStockData()
//            }
//        } else {
//            showToast(getString(R.string.no_internet))
//        }
//    }
//
//    private fun loadNonScheduledVehicleStockData() {
//        if (Util.isNetworkAvailable(this)) {
//
//            var nonScheduledRootId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")
//
//            if (!nonScheduledRootId.equals("", true)) {
//                val loadVanSaleRequest = NonScheduledLoadVanSaleRequest(nonScheduledRootId, this@LoadStockStatusActivity)
//                showLoader()
//                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
//                    hideLoader()
//                    if (isError) {
//                        showToast(getString(R.string.no_vehicle_data_found))
//                    } else {
//                        nonScheduleDos = loadStockMainDo.loadStockDOS
//                        var isProductExisted = false
//                        availableStockDos = nonScheduleDos
//                        for (i in scheduleDos.indices) {
//                            for (k in availableStockDos.indices) {
//                                if (scheduleDos.get(i).product.equals(availableStockDos.get(k).product, true)) {
//                                    availableStockDos.get(k).quantity = availableStockDos.get(k).quantity + scheduleDos.get(i).quantity
//                                    isProductExisted = true
//                                    break
//                                }
//                            }
//                            if (isProductExisted) {
//                                isProductExisted = false
//                                continue
//                            } else {
//                                availableStockDos.add(scheduleDos.get(i))
//                            }
//
//                        }
//                        var loadStockAdapter = LoadStockAdapter(this@LoadStockStatusActivity, availableStockDos, "Shipments")
//                        recycleview.adapter = loadStockAdapter
////                        var unit =preferenceUtils.getStringFromPreference(PreferenceUtils.UNIT_CAPACITY, "")
////
////                        tvCapacity.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, "")+ " "+unit)
////
////                      //  tvNonScheduledStock.setText("Non Scheduled Stock : "+loadStockMainDo.totalNonScheduledStock +" "+loadStockMainDo.vehicleCapacityUnit)
////                        var scheduleTotal = 0.0;
////                        var nonScheduleTotal = 0.0;
//
////                        var scheduleStock =preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_SCHEDULED_STOCK, "")
////                        var nonScheduleStock =loadStockMainDo.totalNonScheduledStock
////                        tvScheduledStock.setText(""+scheduleStock+" "+loadStockMainDo.vehicleCapacityUnit)
////                        tvNonScheduledStock.setText(""+nonScheduleStock+" "+loadStockMainDo.vehicleCapacityUnit)
////
////                        var total = 0.0
////                        tvCapacity.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, "")+" "+unit)
////                        var  capacity=preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY,"")
////                           total=capacity.toDouble()
////                        scheduleTotal=scheduleStock.toDouble()
////                        preferenceUtils.saveString(PreferenceUtils.TOTAL_NON_SCHEDULED_STOCK, loadStockMainDo.totalNonScheduledStock)
////
////                        nonScheduleTotal=nonScheduleStock.toDouble()
////                        tvBalanceStock.setText(""+(total-scheduleTotal-nonScheduleTotal)+" "+ loadStockMainDo.vehicleCapacityUnit)
//
//                    }
//                }
//                loadVanSaleRequest.execute()
//            } else {
////                tvNonScheduledStock.setText("0")
//                var loadStockAdapter = LoadStockAdapter(this@LoadStockStatusActivity, availableStockDos, "Shipments")
//                recycleview.adapter = loadStockAdapter
//            }
//        } else {
//            showToast(getString(R.string.no_internet))
//        }
//    }

    override fun onButtonNoClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
//            finish()

        }
    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            StorageManager.getInstance(this).saveScheduledVehicleStockInLocal(scheduleDos)
            StorageManager.getInstance(this).saveVanScheduleProducts(this@LoadStockStatusActivity, scheduleDos)
            StorageManager.getInstance(this).saveNonScheduledVehicleStockInLocal(nonScheduleDos)
            StorageManager.getInstance(this).saveVanNonScheduleProducts(this@LoadStockStatusActivity, nonScheduleDos)

            var vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
            vehicleCheckInDo.loadStock = resources.getString(R.string.loaded_stock)
            vehicleCheckInDo.checkinTimeCaptureStockLoadingTime = CalendarUtils.getTime()
            vehicleCheckInDo.checkinTimeCaptureStockLoadingDate = CalendarUtils.getDate()

            if (StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)) {
                refreshMenuAdapter()
                setResult(4, null)
                finish()
            } else {
                showToast(getString(R.string.unable_to_load_stock))
            }

        } else
            if (getString(R.string.failure).equals(from, ignoreCase = true)) {


                //finish()
            }

    }

    override fun onFragmentVehicleStockInteraction() {
        Util.startNewFragment(this, R.id.container, VehicleStockFragment.newInstance(), "VehicleStockFragment", false)
    }

    override fun onFragmentOpenStockInteraction() {
        Util.startNewFragment(this, R.id.container, OpenStockFragment.newInstance(), "OpenStockFragment", false)
    }

}