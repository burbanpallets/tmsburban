package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class CreateInvoicePaymentDO implements Serializable {
    public String deliveryCreatedDate = "";
    public String invoiceNumber       = "";
    public String site                = "";
    public int flag                = 0;

    public String siteDescription = "";
    public String siteAddress1 = "";
    public String siteAddress2 = "";
    public String siteAddress3 = "";
    public String siteCountry = "";
    public String siteCity = "";
    public String sitePostalCode = "";
    public String siteLandLine = "";
    public String siteMobile = "";
    public String siteFax = "";
    public String siteEmail1 = "";
    public String siteEmail2 = "";
    public String siteWebEmail = "";    public String company             = "";
    public String supplierName        = "";
    public String customer            = "";
    public String customerDescription = "";
    public String companyCode             = "";
    public String countryname         = "";
    public String website             = "";
    public String landline            = "";
    public String email               = "";
    public String fax                 = "";
    public String mobile              = "";
    public String latitude            = "";
    public String longitude           = "";
    public String salesSite           = "";
    public String arrivalDate         = "";
    public String estimatedDate       = "";
    public String estimatedTime       = "";
    public String margin              = "";
    public String customerTrn         = "";
    public String supplierTrn         = "";
    public String siteStreet          = "";
    public String siteLandMark        = "";
    public String siteTown            = "";
    public String customerStreet      = "";
    public String customerLandMark    = "";
    public String customerTown        = "";
    public String customerCity        = "";
    public String customerPostalCode  = "";


    public String logo                = "";
    public String remarks                = "";



    public String tax                 = "";
    public String taxRate             = "";
    public String taxAmount           = "";
    public String totalGrossAmount    = "";
    public String totalDiscount       = "";
    public String excludingTax        = "";
    public String includingTax        = "";
    public String totalTax            = "";

    public ArrayList<PdfInvoiceDo> pdfInvoiceDos = new ArrayList<>();
    public String createUserName;
    public String createUserID;
    public String createdTime;
    public String createdDate;
    public String paymentTerm;
    public String signature;
    public String deliveryRemarks;
    public String invoiceEmail;

    @Override
    public String toString() {
        return "CreateInvoicePaymentDO{" +
                "deliveryCreatedDate='" + deliveryCreatedDate + '\'' +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", site='" + site + '\'' +
                ", siteDescription='" + siteDescription + '\'' +
                ", company='" + company + '\'' +
                ", supplierName='" + supplierName + '\'' +
                ", customer='" + customer + '\'' +
                ", customerDescription='" + customerDescription + '\'' +
                ", countryname='" + countryname + '\'' +
                ", website='" + website + '\'' +
                ", landline='" + landline + '\'' +
                ", email='" + email + '\'' +
                ", fax='" + fax + '\'' +
                ", mobile='" + mobile + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", salesSite='" + salesSite + '\'' +
                ", arrivalDate='" + arrivalDate + '\'' +
                ", estimatedDate='" + estimatedDate + '\'' +
                ", estimatedTime='" + estimatedTime + '\'' +
                ", margin='" + margin + '\'' +
                ", customerTrn='" + customerTrn + '\'' +
                ", supplierTrn='" + supplierTrn + '\'' +
                ", siteStreet='" + siteStreet + '\'' +
                ", siteLandMark='" + siteLandMark + '\'' +
                ", siteTown='" + siteTown + '\'' +
                ", siteCountry='" + siteCountry + '\'' +
                ", siteCity='" + siteCity + '\'' +
                ", tax='" + tax + '\'' +
                ", taxRate='" + taxRate + '\'' +
                ", taxAmount='" + taxAmount + '\'' +
                ", totalGrossAmount='" + totalGrossAmount + '\'' +
                ", totalDiscount='" + totalDiscount + '\'' +
                ", excludingTax='" + excludingTax + '\'' +
                ", includingTax='" + includingTax + '\'' +
                ", totalTax='" + totalTax + '\'' +
                ", pdfInvoiceDos=" + pdfInvoiceDos +
                '}';
    }
}
