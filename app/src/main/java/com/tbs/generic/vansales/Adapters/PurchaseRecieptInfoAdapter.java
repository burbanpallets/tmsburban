package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.PurchaseRecieptInfoActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.R;

import java.util.ArrayList;

public class PurchaseRecieptInfoAdapter extends RecyclerView.Adapter<PurchaseRecieptInfoAdapter.MyViewHolder> {

    private ArrayList<ActiveDeliveryDO> activeReturnDOS;
    private Context context;
    ActiveDeliveryDO activeDeliveryDo;

    public void refreshAdapter(ActiveDeliveryDO activeDeliveryDO,ArrayList<ActiveDeliveryDO> activeDeliveryDOS) {
        this.activeReturnDOS = activeDeliveryDOS;
        this.activeDeliveryDo=activeDeliveryDO;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvStatus,tvProductDescription,tvLotNumber,tvSerialNumber,tvQuantity;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            tvLotNumber    = view.findViewById(R.id.tvLotNumber);
            tvSerialNumber = view.findViewById(R.id.tvSerialNumber);
            tvQuantity     = view.findViewById(R.id.tvQuantity);
            tvStatus       = view.findViewById(R.id.tvStatus);
            tvProductDescription    = view.findViewById(R.id.tvProductDescription);


        }
    }


    public PurchaseRecieptInfoAdapter(ActiveDeliveryDO activeDeliveryDO,Context context, ArrayList<ActiveDeliveryDO> siteDOS) {
        this.context = context;
        this.activeReturnDOS = siteDOS;
        this.activeDeliveryDo=activeDeliveryDO;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.purchase_lot_added_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final ActiveDeliveryDO activeDeliveryDO = activeReturnDOS.get(position);
        holder.tvLotNumber.setText(activeDeliveryDO.purchaseOrderLotNumber);

        holder.tvProductDescription.setText(activeDeliveryDo.productDescription);
//        holder.tvSerialNumber.setText(activeDeliveryDO.purchaseOrderSerialNumber);
        holder.tvQuantity.setText(""+activeDeliveryDO.purchaseOrderQuantity +" "+ activeDeliveryDo.unit);
        holder.tvStatus.setText(activeDeliveryDO.purchaseOrderStatus);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PurchaseRecieptInfoActivity)context).showAddItemDialog(position+"",activeDeliveryDO.purchaseOrderLotNumber,
                        activeDeliveryDO.purchaseOrderSerialNumberStart, activeDeliveryDO.purchaseOrderSerialNumberEnd,
                        activeDeliveryDO.purchaseOrderQuantity,activeDeliveryDO.purchaseOrderStatus);
            }
        });



    }

    @Override
    public int getItemCount() {
        return activeReturnDOS.size();
    }

}
