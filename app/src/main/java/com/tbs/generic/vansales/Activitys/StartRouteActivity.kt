package com.tbs.generic.vansales.Activitys

import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.R

//
class StartRouteActivity : BaseActivity() {

    override fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = layoutInflater.inflate(R.layout.start_route, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            setResult(13, null)
            finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.start_route)
        showAppCompatAlert(getString(R.string.directions), getString(R.string.start_nav), getString(R.string.start), getString(R.string.cancel), getString(R.string.success), false)


    }
    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
//            val intent = Intent(this@StartRouteActivity, Navigation::class.java)
//            startActivity(intent)


        }
    }

    override fun onButtonNoClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
          finish()

        }
    }

    }