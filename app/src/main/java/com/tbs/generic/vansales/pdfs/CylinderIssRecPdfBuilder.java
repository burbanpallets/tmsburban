package com.tbs.generic.vansales.pdfs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.Model.CylinderIssueDO;
import com.tbs.generic.vansales.Model.CylinderIssueMainDO;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.pdfs.utils.PDFConstants;
import com.tbs.generic.vansales.pdfs.utils.PDFOperations;
import com.tbs.generic.vansales.pdfs.utils.PreviewActivity;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Kishore on 12-1-2019.
 * Copyright (C) 2018 TBS - All Rights Reserved
 */
public class CylinderIssRecPdfBuilder {

    private static Context context;
    private Document document = null;
    private PdfPTable parentTable;

    private Font normalFont, boldFont;

    private CylinderIssRecPdfBuilder() {

    }

    public static CylinderIssRecPdfBuilder getBuilder(Context mContext) {
        //create pdf here
        context = mContext;
        return new CylinderIssRecPdfBuilder();
    }


    private CylinderIssueMainDO cylinderIssueMainDO;

    @SuppressLint("StaticFieldLeak")
    public CylinderIssRecPdfBuilder build(CylinderIssueMainDO cylinderIssueMainDO, String type) {
        this.cylinderIssueMainDO = cylinderIssueMainDO;
        Log.d("cylinderIssueMainDO->", cylinderIssueMainDO + "");
        new AsyncTask<Void, Void, Void>() {

            ProgressDialog progressDialog = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//                progressDialog.setCancelable(false);
                progressDialog.setMessage("Generating Cylinder Issue Reciept Please wait...");
//                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    parentTable = new PdfPTable(1);
                    parentTable.setWidthPercentage(100);
                    init(type);
                    //addLogo();
                    addHeaderLogo();
                    addHeaderLabel();
                    addCustomerDetailsToPdf();
                    addProductsTableLabels();
                    addProductsToPdf();
                    addSignature();
                    addFooterToPdf();
                    document.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                progressDialog.cancel();
                sendPDfewDoc();
            }
        }.execute();
        return this;
    }
    @SuppressLint("StaticFieldLeak")
    public CylinderIssRecPdfBuilder createPDF(CylinderIssueMainDO cylinderIssueMainDO, String type) {
        this.cylinderIssueMainDO = cylinderIssueMainDO;
        Log.d("cylinderIssueMainDO->", cylinderIssueMainDO + "");
        new AsyncTask<Void, Void, Void>() {

            ProgressDialog progressDialog = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Preparing Cylinder Issue Reciept Please wait...");
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    parentTable = new PdfPTable(1);
                    parentTable.setWidthPercentage(100);
                    init(type);
                    //addLogo();
                    addHeaderLogo();
                    addHeaderLabel();
                    addCustomerDetailsToPdf();
                    addProductsTableLabels();
                    addProductsToPdf();
                    addSignature();
                    addFooterToPdf();
                    document.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressDialog.cancel();
                Intent intent = new Intent(context.getApplicationContext(), PreviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("KEY",PDFConstants.CYLINDER_ISS_REC_PDF_NAME);
                context.startActivity(intent);
            }
        }.execute();
        return this;
    }

    private void init(String type) {
        boldFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);
        normalFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.NORMAL, BaseColor.BLACK);
        if(type.equalsIgnoreCase("Preview")){
            normalFont = boldFont;
        }
        File file = new File(Util.getAppPath(context) + PDFConstants.CYLINDER_ISS_REC_PDF_NAME);
        if (file.exists())
            file.delete();
        try {
            document = new Document();
            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));
            // Open to write
            document.open();
            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("TBS");
            document.addCreator("Venu Appasani");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addHeaderLogo() {
        try {
            document.add(PDFOperations.getInstance().
                    getHeadrLogoIOSCertificateLogo(context, document,cylinderIssueMainDO.companyCode));
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    private void addHeaderLabel() {
        try {
            Font headerLabelFont = new Font(Font.FontFamily.COURIER, 11.0f, Font.BOLD, BaseColor.BLACK);
            Paragraph headerLabel = new Paragraph(cylinderIssueMainDO.company, headerLabelFont);
            headerLabel.setAlignment(Element.ALIGN_CENTER);
            headerLabel.setPaddingTop(10);
            document.add(headerLabel);
            Paragraph headerLabel1 = new Paragraph("Cylinder Issuance/Receipt Document", headerLabelFont);
            headerLabel1.setAlignment(Element.ALIGN_CENTER);
            headerLabel1.setPaddingTop(10);
            document.add(headerLabel1);
            addEmptySpaceLine(1);
        } catch (Exception e) {
            addEmptySpaceLine(1);
            e.printStackTrace();
        }
    }

    private void addCustomerDetailsToPdf() {
        try {

            PdfPTable headerParentTable = new PdfPTable(1);
            headerParentTable.setPaddingTop(10f);
            headerParentTable.setWidthPercentage(100);

            float[] columnWidths = {2.2f, 0.3f, 6, 1.5f, 0.3f, 3f};
            PdfPTable headerTable = new PdfPTable(columnWidths);
            headerTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellOne1 = new PdfPCell();
            cellOne1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellTwo = new PdfPCell();
            cellTwo.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree = new PdfPCell();
            cellThree.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree1 = new PdfPCell();
            cellThree1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellFour = new PdfPCell();
            cellFour.setBorder(Rectangle.NO_BORDER);


            cellOne.addElement(new Phrase(PDFConstants.D_R_NO, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.DELIVERY_TO, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.ADDRESS, boldFont));

            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));

            cellTwo.addElement(new Phrase(cylinderIssueMainDO.shipmentNumber, normalFont));
            cellTwo.addElement(new Phrase(cylinderIssueMainDO.customerDescription, normalFont));
            cellTwo.addElement(new Phrase(getAddress(), normalFont));

            cellThree.addElement(new Phrase(PDFConstants.DATE, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.TIME, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_ID, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_NAME, boldFont));

            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            String dMonth = cylinderIssueMainDO.createdDate.substring(4, 6);
            String dyear  = cylinderIssueMainDO.createdDate.substring(0, 4);
            String dDate  = cylinderIssueMainDO.createdDate.substring(Math.max(cylinderIssueMainDO.createdDate.length() - 2, 0));

            cellFour.addElement(new Phrase(dDate + "-" + dMonth + "-" + dyear, normalFont));
            cellFour.addElement(new Phrase(cylinderIssueMainDO.createdTime, normalFont));
//            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, ""), normalFont));
//            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, ""), normalFont));
            cellFour.addElement(new Phrase(cylinderIssueMainDO.createUserID, normalFont));
            cellFour.addElement(new Phrase(cylinderIssueMainDO.createUserName, normalFont));
            headerTable.addCell(cellOne);
            headerTable.addCell(cellOne1);
            headerTable.addCell(cellTwo);
            headerTable.addCell(cellThree);
            headerTable.addCell(cellThree1);
            headerTable.addCell(cellFour);
            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.addElement(headerTable);
            headerParentTable.addCell(pdfPCell);
            document.add(headerParentTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    private void addProductsTableLabels() {
        try {

            float[] productColumnsWidth = {2.5f, 10, 3, 3, 3, 3};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            PdfPCell cellTwo = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            PdfPCell cellFour = new PdfPCell();
            PdfPCell cellFive = new PdfPCell();
            PdfPCell cellSix = new PdfPCell();

            Paragraph pOne = new Paragraph("Sr. No.", boldFont);
            pOne.setAlignment(Element.ALIGN_CENTER);
            cellOne.addElement(pOne);

            Paragraph pTwo = new Paragraph("Item", boldFont);
            pTwo.setAlignment(Element.ALIGN_LEFT);
            cellTwo.addElement(pTwo);

            Paragraph pThree = new Paragraph("Opening QTY with Customer", boldFont);
            pThree.setAlignment(Element.ALIGN_CENTER);
            cellThree.addElement(pThree);

            Paragraph pFour = new Paragraph("Issued Quantity", boldFont);
            pFour.setAlignment(Element.ALIGN_CENTER);
            cellFour.addElement(pFour);

            Paragraph pFive = new Paragraph("Received Quantity", boldFont);
            pFive.setAlignment(Element.ALIGN_CENTER);
            cellFive.addElement(pFive);

            Paragraph pSix = new Paragraph("Balance with Customer", boldFont);
            pSix.setAlignment(Element.ALIGN_CENTER);
            cellSix.addElement(pSix);


            productsTable.addCell(cellOne);
            productsTable.addCell(cellTwo);
            productsTable.addCell(cellThree);
            productsTable.addCell(cellFour);
            productsTable.addCell(cellFive);
            productsTable.addCell(cellSix);

            document.add(productsTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addProductsToPdf() {

        try {

            float[] productColumnsWidth = {2.5f, 10, 3, 3, 3, 3};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);

            if (cylinderIssueMainDO.cylinderIssueDOS != null &&
                    cylinderIssueMainDO.cylinderIssueDOS.size() > 0) {
                for (int i = 0; i < cylinderIssueMainDO.cylinderIssueDOS.size(); i++) {
                    CylinderIssueDO cylinderIssueDO = cylinderIssueMainDO.cylinderIssueDOS.get(i);
                    PdfPCell cellOne = new PdfPCell();
                    PdfPCell cellTwo = new PdfPCell();
                    PdfPCell cellThree = new PdfPCell();
                    PdfPCell cellFour = new PdfPCell();
                    PdfPCell cellFive = new PdfPCell();
                    PdfPCell cellSix = new PdfPCell();

                    cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prOne = new Paragraph("" + (i + 1), normalFont);
                    prOne.setAlignment(Element.ALIGN_CENTER);
                    cellOne.addElement(prOne);
                    cellOne.addElement(new Phrase("\n"));

                    cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cellTwo.addElement(new Phrase("" + cylinderIssueDO.productDescription, normalFont));
                    cellTwo.addElement(new Phrase("\n"));

                    cellThree.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prThree = new Paragraph("" + cylinderIssueDO.orderedQuantity, normalFont);
                    prThree.setAlignment(Element.ALIGN_CENTER);
                    cellThree.addElement(prThree);
                    cellThree.addElement(new Phrase("\n"));

                    cellFour.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prFour = new Paragraph("" + cylinderIssueDO.issuedQuantity, normalFont);
                    prFour.setAlignment(Element.ALIGN_CENTER);
                    cellFour.addElement(prFour);
                    cellFour.addElement(new Phrase("\n"));

                    cellFive.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prFive = new Paragraph("" + cylinderIssueDO.recievedQuantity, normalFont);
                    prFive.setAlignment(Element.ALIGN_CENTER);
                    cellFive.addElement(prFive);
                    cellFive.addElement(new Phrase("\n"));

                    cellSix.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prSix = new Paragraph("" + cylinderIssueDO.balanceQuantity , normalFont);
                    prSix.setAlignment(Element.ALIGN_CENTER);
                    cellSix.addElement(prSix);
                    cellSix.addElement(new Phrase("\n"));

                    productsTable.addCell(cellOne);
                    productsTable.addCell(cellTwo);
                    productsTable.addCell(cellThree);
                    productsTable.addCell(cellFour);
                    productsTable.addCell(cellFive);
                    productsTable.addCell(cellSix);
                }
                document.add(productsTable);

                PdfPTable totalTable = new PdfPTable(1);
                totalTable.setWidthPercentage(100);

                PdfPCell remCell = new PdfPCell();
                PdfPTable remTable = new PdfPTable(1);
                PdfPCell cellOne1 = new PdfPCell();
                remCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cellOne1.setBorder(Rectangle.NO_BORDER);
                cellOne1.setHorizontalAlignment(Element.ALIGN_LEFT);


              /*  Paragraph remPara = new Paragraph("Remarks:-", boldFont);
                remPara.setAlignment(Element.ALIGN_LEFT);
                cellOne1.addElement(remPara);
                cellOne1.addElement(new Phrase("\n"));
                cellOne1.addElement(new Phrase("\n"));
                remTable.addCell(cellOne1);*/

                PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
                String note = "";
                if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
                    note = podDo.getNotes();
                }
                PdfPCell noteCell = new PdfPCell(new Phrase("Remarks : " + note, boldFont));
//                PdfPCell noteCell = new PdfPCell(new Phrase("AED : Eighteen Thousand Eight Hundred Forty Seven and 50/100 Only-", boldFont));
                noteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                noteCell.setPadding(10);
                totalTable.addCell(noteCell);

                remCell.addElement(remTable);
                totalTable.addCell(remCell);
                document.add(totalTable);


            }
            addEmptySpaceLine(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void addSignature() {
        try {
//

            float[] productColumnsWidth1 = {2, 6};
            PdfPTable sig = new PdfPTable(productColumnsWidth1);
            sig.setWidthPercentage(100);
            Image image = PDFOperations.getInstance().getSignatureFromFile(cylinderIssueMainDO.signature);
            if(image !=null){
                image.setBorder(Rectangle.NO_BORDER);
                PdfPCell pdfPCell = new PdfPCell();
                pdfPCell.setBorder(Rectangle.NO_BORDER);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
                pdfPCell.addElement(image);
                sig.addCell(pdfPCell);
                PdfPCell pCell = new PdfPCell();
                pCell.setBorder(Rectangle.NO_BORDER);
                sig.addCell(pCell);
                document.add(sig);
            }

            PdfPTable custTable = new PdfPTable(1);
            custTable.setWidthPercentage(100);


            PdfPCell pdfPCellSig = new PdfPCell(new Phrase("Customer's Signature:", normalFont));
            pdfPCellSig.setBorder(Rectangle.NO_BORDER);
            pdfPCellSig.setHorizontalAlignment(Element.ALIGN_LEFT);
            custTable.addCell(pdfPCellSig);
            document.add(custTable);

            addEmptySpaceLine(1);


        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }


    private void addFooterToPdf() {
        try {
            //document.add(PDFOperations.getInstance().addFooterImage(context, document));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void addEmptySpaceLine(int noOfLines) {
        try {
            PdfPTable emptyTable = new PdfPTable(1);
            emptyTable.setWidthPercentage(100);
            for (int i = 0; i < noOfLines; i++) {
                PdfPCell emptyCell = new PdfPCell();
                emptyCell.setBorder(Rectangle.NO_BORDER);
                emptyCell.addElement(new Paragraph("\n"));
                emptyTable.addCell(emptyCell);
            }
            document.add(emptyTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }


    private void sendPDfewDoc() {
        String shipmentType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "");
        if (shipmentType.equalsIgnoreCase("Scheduled")) {
            ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(context).getActiveDeliveryMainDo(context);
            if(activeDeliverySavedDo.cylinderIssueEmail.length()>0){
                sendpdfMail(activeDeliverySavedDo.cylinderIssueEmail, activeDeliverySavedDo.customerDescription);
            }else {
                ((BaseActivity)context).showToast("Please provide email address");
            }

        } else {
            CustomerDo customerDo = StorageManager.getInstance(context).getCurrentSpotSalesCustomer(context);
            if(customerDo.cylinderIssueEmail.length()>0){
                sendpdfMail(customerDo.cylinderIssueEmail, customerDo.customerName);
            }else {
                ((BaseActivity)context).showToast("Please provide email address");
            }

        }

    }

    private void sendpdfMail(String email, String userName) {
       /* TBSMailBG.newBuilder(context, "CYL Delivert Note")
                .withUsername("online@brothersgas.ae")
                .withPassword("Cam52743@2018")
                .withMailto(email)
//                .withMailto("tbs.vansales@gmail.com")
                .withSubject()
                .withBody("Dear " + userName + ", Please find the attached Cylinder Issuance/Receipt Document for your reference.")
                .withAttachments(Util.getAppPath(context) + "CylinderIssRec.pdf")

                .send();
*/
        PDFOperations.getInstance().sendpdfMail(context,
                email,
                userName,
                PDFConstants.CYLINDER_ISS_REC_PDF_NAME, PDFConstants.CYLINDER_ISS_REC);
    }

    private String getAddress() {


        String street = cylinderIssueMainDO.customerStreet;
        String landMark = cylinderIssueMainDO.customerLandMark;
        String town = cylinderIssueMainDO.customerTown;
        String postal = cylinderIssueMainDO.customerPostalCode;
        String city = cylinderIssueMainDO.customerCity;
        String countryName = cylinderIssueMainDO.countryName;

        String finalString = "";

        if (!TextUtils.isEmpty(street)) {
            finalString += street + ", ";
        }
        if (!TextUtils.isEmpty(landMark)) {
            finalString += landMark + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
//        if (!TextUtils.isEmpty(countryName)) {
//            finalString += countryName;
//        }

        return finalString;
    }
}
