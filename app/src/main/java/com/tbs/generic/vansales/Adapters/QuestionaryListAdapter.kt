package com.tbs.generic.vansales.Adapters

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tbs.generic.vansales.Model.InspectionDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.dialogs.AddViewNoteDialogFragment
import com.tbs.generic.vansales.listeners.ResultListner
import kotlinx.android.synthetic.main.item_inspection_questionary.view.*
import java.util.*


class QuestionaryListAdapter(
    private val context: Context,
    internal var questions: ArrayList<InspectionDO>?
) :
    RecyclerView.Adapter<QuestionaryListAdapter.MyViewHolder>() {


    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val addnote: LinearLayout = view.ll_add_note
        val tvAddNote: TextView = view.tv_addnote
        val tv_question: TextView = view.tv_question
        val checkbox: CheckBox = view.checkbox
        val rlQuestion: RelativeLayout = view.rl_question
        val tvCode = view.tv_code

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_inspection_questionary, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var inspectionDO = questions?.get(position)

        if (inspectionDO != null) {
            holder.tv_question.setText("" + questions!!.get(position).questionDes)
            holder.tvCode.setText(questions!!.get(position).question)

            if (inspectionDO.ischecked) {
                holder.checkbox.setChecked(true)
                holder.addnote.visibility = View.GONE
            } else {
                holder.addnote.visibility = View.VISIBLE
                if (!TextUtils.isEmpty(inspectionDO.note)) {
                    holder.tvAddNote.text = context.getString(R.string.view_note)
                } else {
                    holder.tvAddNote.text = context.getString(R.string.add_note)
                }
                holder.checkbox.setChecked(false)

            }

            holder.rlQuestion.setOnClickListener {
                if (inspectionDO.ischecked) {
                    holder.checkbox.setChecked(false)
                    inspectionDO.ischecked = false
                    holder.addnote.visibility = View.VISIBLE
                    holder.tvAddNote.text = context.getString(R.string.add_note)
                } else {
                    holder.checkbox.setChecked(true)
                    inspectionDO.ischecked = true

//                    inspectionDO.note = ""
                    holder.addnote.visibility = View.GONE
                    holder.tvAddNote.text = context.getString(R.string.view_note)

                }


            }

            holder.addnote.setOnClickListener {
                Log.d("note-->", inspectionDO.toString())
                val note = inspectionDO.note + ""
                Log.d("note-->", note)
                AddViewNoteDialogFragment().newInstance(
                    "Alert",
                    note,
                    ResultListner { `object`, isSuccess ->
                        if (isSuccess) {
                            inspectionDO.note = `object`.toString()
                            holder.tvAddNote.text = context.getString(R.string.view_note)
                        }

                    }).show(
                    (context as AppCompatActivity).supportFragmentManager,
                    "AddViewNoteDialogFragment"
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return questions!!.size
    }

    fun setChecked(checked: Boolean) {

        for (ispDo: InspectionDO in this.questions!!) {
            ispDo.ischecked = checked

        }
        notifyDataSetChanged()
    }

}
