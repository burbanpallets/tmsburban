package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.generic.vansales.Model.CustomerReturnDO;
import com.tbs.generic.vansales.Model.MiscStopDO;
import com.tbs.generic.vansales.Model.PickTicketDO;
import com.tbs.generic.vansales.Model.PurchaseReceiptDO;
import com.tbs.generic.vansales.Model.PurchaseReturnDO;
import com.tbs.generic.vansales.Model.UserDeliveryDO;
import com.tbs.generic.vansales.R;

import java.util.ArrayList;

public class UserActivityCusReturnAdapter extends RecyclerView.Adapter<UserActivityCusReturnAdapter.MyViewHolder> {

    private ArrayList<CustomerReturnDO> customerReturnDOS;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRecieptNumber, tvCustomerName;
        private TextView tvAmount;

        public MyViewHolder(View view) {
            super(view);
            tvAmount = view.findViewById(R.id.tvAmount);
            tvRecieptNumber = view.findViewById(R.id.tvRecieptNumber);
            tvCustomerName = view.findViewById(R.id.tvCustomerName);


        }
    }


    public UserActivityCusReturnAdapter(Context context, ArrayList<CustomerReturnDO> siteDOS) {
        this.context = context;
        this.customerReturnDOS = siteDOS;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_data_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final CustomerReturnDO customerReturnDO = customerReturnDOS.get(position);

        String amount = String.valueOf(customerReturnDO.quantity);

        holder.tvRecieptNumber.setText(customerReturnDO.documentNumber);
        holder.tvCustomerName.setText(customerReturnDO.customerName);
        holder.tvAmount.setText("" + amount);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });


    }

    @Override
    public int getItemCount() {
        return customerReturnDOS.size();
    }

}
