//package com.tbs.generic.vansales.Adapters;
//
///**
// * Created by sandy on 2/7/2018.
// */
//
//import android.content.Context;
//import android.content.Intent;
//import androidx.recyclerview.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.tbs.generic.vansales.Activitys.ProductDetailsActivity;
//import com.tbs.generic.vansales.Model.ProductDO;
//import com.tbs.generic.vansales.R;
//
//import org.jetbrains.annotations.NotNull;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder>  {
//
//    private List<ProductDO> productDOS;
//    private Context context;
//
//    public void refreshAdapter(@NotNull ArrayList<ProductDO> productDOs) {
//        this.productDOS = productDOs;
//        notifyDataSetChanged();
//    }
//
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//        public TextView tvProductName, tvProductId;
//        private LinearLayout llDetails;
//
//        public MyViewHolder(View view) {
//            super(view);
//            llDetails = view.findViewById(R.id.llDetails);
//            tvProductName = view.findViewById(R.id.tvProductName);
//            tvProductId = view.findViewById(R.id.tvProductId);
//
//
//
//        }
//    }
//
//
//    public ProductAdapter(Context context, List<ProductDO> productDOS) {
//        this.context = context;
//        this.productDOS = productDOS;
//    }
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.product_data, parent, false);
//
//        return new MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(final MyViewHolder holder, int position) {
//
//        final ProductDO productDO = productDOS.get(position);
//        holder.tvProductName.setText(productDO.productName);
//        holder.tvProductId.setText("" + productDO.productId);
//
//        holder.llDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = new Intent(context, ProductDetailsActivity.class);
//                intent.putExtra("Code", productDO.productId);
//                context.startActivity(intent);
//
//
//            }
//        });
//
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return productDOS.size();
//    }
//
//}
